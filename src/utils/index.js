export const uuid = () => {
  const s = []
  const hexDigits = '0123456789abcdef'
  for (let i = 0; i < 36; i++) {
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1)
  }
  s[14] = '4'
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1)
  s[8] = s[13] = s[18] = s[23] = '-'

  const uuid = s.join('')
  return uuid
}

export const formatstamp = stamp => {
  let result = ''
  if (/^\d{10,}$/.test(stamp.toString())) {
    const date = new Date(stamp)
    const y = date.getFullYear()
    const m = '0' + (date.getMonth() + 1)
    const d = '0' + date.getDate()
    result = y + '-' + m.slice(-2) + '-' + d.slice(-2)
  }
  return result
}

export const orderBy = data => {
  if (!data.every(item => item.order)) return data
  return data.sort((prev, next) => prev.order - next.order)
}

export const searchCd = (...args) => {
  const keyword = args.shift()
  const result = []
  for (let i = 0; i < args.length; i++) {
    result.push(new RegExp(keyword, 'ig').test(args[i]))
  }
  return result.some(val => val)
}

export const update = (data, form, cases = ['password']) => {
  for (let i = 0; i < data.length; i++) {
    if (data[i].id === form.id) {
      for (const key in form) {
        const exist = key in data[i]
        const equal = form[key] === data[i][key]
        if (cases.includes(key) && !form[key]) continue
        if (exist && !equal) data[i][key] = form[key]
      }
      continue
    }
  }
}

export const deepCopy = (target, newObj = {}) => {
  for (const i in target) {
    if (typeof target[i] === 'object') {
      newObj[i] = Array.isArray(target[i]) ? [] : {}
      deepCopy(target[i], newObj[i])
    } else {
      newObj[i] = target[i]
    }
  }
  return newObj
}
